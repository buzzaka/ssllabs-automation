require('dotenv').config()

const appRootDir = require('app-root-dir').get();
const request = require('request');
const chalk = require('chalk');
const terminalLink = require('terminal-link');
const { IncomingWebhook } = require('@slack/client');
const log = console.log;

const terminalLinkSite = terminalLink(process.env.PROJECT_URL, process.env.PROJECT_URL);
const resultLink = `${process.env.ANALYZE_URL}?d=${process.env.PROJECT_URL}`;
const webhook = new IncomingWebhook(process.env.SLACK_WEBHOOK_URL);
var endpoint = 'analyze';
var interval = 10000;

log(chalk.magenta.bold('***** SSL LABS automation *****'));

request(`${process.env.API_URL}/${endpoint}?host=${process.env.PROJECT_URL}&startNew=on&ignoreMismatch=on&all=on`, { json: true }, (err, res, body) => {
    if (err) return err;
    log(chalk.cyan(`[INIT] Start new assessment for ${terminalLinkSite}`));
    processResponse(body);
});

function processResponse(body) {
    switch (body.status) {
        case 'READY':
            var warnings = false;
            const terminalLinkAssessment = terminalLink(resultLink, resultLink);
            log(chalk.green(`[${body.status}] ${body.endpoints[0].statusMessage}`));
            log(chalk.white(`Overall Rating : ${body.endpoints[0].grade}`));
            if (body.endpoints[0].hasWarnings) {
                warnings = true;
                log(chalk.white(`Some security issues has been detected`));
            }
            log(chalk.green.bold(`Full assessment analyze for ${terminalLinkSite} is now available at ${terminalLinkAssessment}`));
            processResponseEnd(body.endpoints[0].grade, warnings);
            break;
        case 'ERROR':
            log(chalk.red(`[${body.status}] Something went wrong, please try again later`));
            processResponseEnd();
            break;
        default: // IN_PROGRESS
            log(chalk.white(`[${body.status}] ${body.statusMessage ? body.statusMessage : (body.endpoints ? body.endpoints[0].statusDetailsMessage : '')}`));
            log(chalk.white(`Next check in ${interval/1000} seconds...`));
            break;
    }
}

function processResponseEnd(grade, warnings) {
    sendSlackNotification(process.env.PROJECT_URL, resultLink, grade, warnings);
    if (timer) {
        clearInterval(timer);
        return;
    }
}

var timer = setInterval(function() {
    request(`${process.env.API_URL}/${endpoint}?host=${process.env.PROJECT_URL}&ignoreMismatch=on&all=on`, { json: true }, (err, res, body) => {
        processResponse(body);
    });
}, interval);

function sendSlackNotification(projectUrl, analyseUrl, grade, warnings) {
    var errorMessage = '';
    if (warnings) {
        errorMessage = 'Some security issues has been detected. ';
    }
    const message = `Overall Rating : ${grade}. ${errorMessage}Assessment analyze for ${projectUrl} is now available, <${analyseUrl}|Click here> for details`;
    webhook.send(message, function(err, res) {
        if (err) {
            console.log('Error:', err);
        } else {
            console.log('Message sent: ', message);
        }
    });
}
