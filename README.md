# SSLLabs Automation

## Install on external project

Run the following command: `npm install --save-dev ssh://git@bitbucket.org:buzzaka/ssllabs-automation.git`

Then, add this configuration file at the root of your project, named `ssllabs.json`:

```
{
  "project_url": "yourprodsite.co",
  "api_url": "https://api.ssllabs.com/api/v3/",
  "analyze_url": "https://www.ssllabs.com/ssltest/analyze.html",
  "slack_webhook_url": "https://hooks.slack.com/services/XXXXXX/XXXXXX/XXXXXXXXX"
}
```

Get your Slack Webhook URL from Slack [create app](https://api.slack.com/apps) in the Incoming webhooks section.
The Slack app should be connected to a channel where you want all notifications to appear.
